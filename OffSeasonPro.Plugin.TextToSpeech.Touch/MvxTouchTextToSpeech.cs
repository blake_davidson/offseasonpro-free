using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using MonoTouch.AVFoundation;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Diagnostics;

namespace OffSeasonPro.Plugin.TextToSpeech.Touch
{
    public class MvxTouchTextToSpeech : ITextToSpeech
    {

        private AVAudioPlayer _player;
        private AVSpeechSynthesizer _speechSynthesizer;
        private Action _finished;

        public void Play(string text, Action finished)
        {
            if (UIDevice.CurrentDevice.CheckSystemVersion(7, 0))
            {
                var speechUtterance = new AVSpeechUtterance(text) { Rate = 0.15f, PitchMultiplier = 0.9f };
                _finished = finished;
                _speechSynthesizer.SpeakUtterance(speechUtterance);
            }
            else
            {
                PlayMp3(text.Replace(@" ", @"") + ".mp3", finished);
            }

        }

        public MvxTouchTextToSpeech()
        {
            Texts = new Dictionary<string, string>();
            if (!UIDevice.CurrentDevice.CheckSystemVersion(7, 0))
            {
                var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                var localPath = Path.Combine(documentsPath, @"TTS");

                //Create TTS Directory if one does not exist.
                if (!Directory.Exists(localPath))
                {
                    Directory.CreateDirectory(localPath);
                }

                NSError obj = null;
                AVAudioSession.SharedInstance().SetCategory(AVAudioSession.CategoryPlayback, out obj);
            }
            else
            {
                _speechSynthesizer = new AVSpeechSynthesizer();
                _speechSynthesizer.DidFinishSpeechUtterance += (sender, args) => _finished();

            }
        }

		public void TextToSpeech(Dictionary<string, string> textsAndFiles, Action Finished, Action ErrorOccured)
        {
			if (!UIDevice.CurrentDevice.CheckSystemVersion (7, 0)) {

				foreach (var textAndFile in textsAndFiles) {
					var fileName = textAndFile.Value;
					var text = textAndFile.Key;
			
					var documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
					var localPath = Path.Combine (documentsPath, @"TTS");
					localPath = Path.Combine (localPath, fileName);
					var success = false;

					if (!File.Exists (localPath)) {
						var webClient = new WebClient ();
						try {
							webClient.DownloadFile ("http://www.translate.google.com/translate_tts?tl=en&q=" + text, localPath);
							success = true;
						} catch (Exception ex) {
							ErrorOccured ();
							return;
						}
					} else {
						success = true;
					}

					if (success && !Texts.ContainsKey (fileName))
						Texts.Add (fileName, localPath);
				}
			}

			Finished ();

	    }

        private void PlayMp3(string fileName, Action finished)
        {

            if (Texts.ContainsKey(fileName))
            {
                var filePath = Texts[fileName];

                //Load Audio
                _player = AVAudioPlayer.FromUrl(new NSUrl(filePath));
                //Setup Audio Player
                _player.Volume = 0.95f;
                _player.FinishedPlaying += (sender, e) => finished();
                _player.PrepareToPlay();
                _player.BeginInterruption += (sender, args) => Console.WriteLine("PLAYER BEGIN INTERUPTION!");
                _player.EndInterruption += (sender, args) => Console.WriteLine("PLAYER END INTERUPTION!");

                _player.Play();
            }
            else
            {
                finished();
            }

        }

        public Dictionary<string, string> Texts { get; set; }

    }
}
