using Cirrious.CrossCore;
using Cirrious.CrossCore.Plugins;

namespace OffSeasonPro.Plugin.TextToSpeech.Touch
{
    public class Plugin
        : IMvxPlugin
    {
        public void Load()
        {
            Mvx.RegisterSingleton<ITextToSpeech>(new MvxTouchTextToSpeech());
        }
    }
}