using System;
using System.Text;
using System.Threading;
using System.Windows.Input;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.MvvmCross.ViewModels;
using OffSeasonPro.Core.Messages;
using OffSeasonPro.Core.Models;
using OffSeasonPro.Plugin.MusicPlayerPlugin;
using OffSeasonPro.Plugin.TextToSpeech;

namespace OffSeasonPro.Core.ViewModels
{
    public class WorkoutViewModel
        : BaseViewModel
    {
        private readonly IMusicPlayer _player;
		private bool _buttonsEnabled;
        private readonly AutoResetEvent _reset = new AutoResetEvent(false);
        private readonly ITextToSpeech _tts;
        private readonly IMvxMessenger _messenger;
        private readonly Timer _workoutClock;

		private bool _workingout;
		private bool _workoutReset;
		private bool _workoutComplete;
		private float _clockValue;
		private string _btnPausePlayText;
		private int _maxProgressValue;
		private string _currentPostion;
		private bool _playing;

        public WorkoutViewModel(ITextToSpeech tts, IMvxMessenger messenger, IMusicPlayer player)
        {
            //Set up timer
            _workoutClock = new Timer(Workout_Timer_Tick, _reset, Timeout.Infinite, Timeout.Infinite);
            _player = player;
            _tts = tts;
            _messenger = messenger;

			_workingout = true;
			_workoutReset = false;
            MaxProgressValue = Settings.SetLengthSeconds.ToInt32();
            BtnPausePlayText = "Pause";
        }

	
		private bool _adMobLoaded;
		public bool AdMobLoaded {
			get { return _adMobLoaded; }
			set { _adMobLoaded = value; }
		}

		private bool _viewingAd;
		public bool ViewingAd {
			get {return _viewingAd; }
			set { _viewingAd = value; }
		}

        public Settings Settings
        {
            get { return DataStore.Settings; }
        }

        public ICommand BeginWorkoutCommand
        {
            get { return new MvxCommand(UpdateWorkout); }
        }

        public ICommand PauseResumeWorkout
        {
            get { return new MvxCommand(PauseOrResume); }
        }

        public ICommand StartMusic
        {
            get { return new MvxCommand(() => _player.Start()); }
        }

        public ICommand Reset
        {
            get { return new MvxCommand(ResetWorkout); }
        }

        public ICommand GoHome
        {
            get { return new MvxCommand(ReturnToHome); }
        }

        private void ReturnToHome()
        {
            if (!Settings.IsPaid && !_workoutComplete)
                DisplayAd();

			if(AdMobLoaded)
				CloseCommand.Execute (null);          
        }

		public bool Playing
		{
			get { return _playing; }
		}

		private bool _resizeLabel;
		public bool ResizeLabel {
			get { return _resizeLabel; }
			set { RaisePropertyChanged (() => ResizeLabel); }
		}

		public int DigitCount 
		{
			get 
			{
				var digits = 0;
				var holdDigits = 0;
				digits = (int)Math.Floor(Math.Log10((float)Settings.SetLengthSeconds.ToInt32()) + 1);
				holdDigits = (int)Math.Floor(Math.Log10((float)Settings.TransitionLengthSeconds.ToInt32()) + 1);

				if(holdDigits > digits)
					digits = holdDigits;

				holdDigits = (int)Math.Floor(Math.Log10((float)Settings.StartupLength.ToInt32()) + 1);

				if(holdDigits > digits)
					digits = holdDigits;

				return digits;

			}

		}

        public float ClockValue
        {
            get { return _clockValue; }
            set
            {
                _clockValue = value;
                RaisePropertyChanged(() => ClockValue);
            }
        }

        public string BtnPausePlayText
        {
            get { return _btnPausePlayText; }
            set
            {
                _btnPausePlayText = value;
                RaisePropertyChanged(() => BtnPausePlayText);
            }
        }

        public int MaxProgressValue
        {
            get { return _maxProgressValue; }
            set
            {
                _maxProgressValue = value;
                RaisePropertyChanged(() => MaxProgressValue);
            }
        }

        public string CurrentPosition
        {
            get { return _currentPostion; }
            set
            {
                _currentPostion = value;
                RaisePropertyChanged(() => CurrentPosition);
				ResizeLabel = true;
            }
        }

		public bool ButtonsEnabled 
		{
			get { return _buttonsEnabled; }
			private set 
			{
				_buttonsEnabled = value;
				RaisePropertyChanged (() => ButtonsEnabled);
			}
		}

        private void UpdateWorkout()
        {
            var sb = new StringBuilder();
            if (!_workoutComplete)
            {
                try
                {
                    if (Settings.CurrentWorkout.WarmingUp)
                    {
                        sb.Append("Warm up");
                        MaxProgressValue = Settings.StartupLength.ToInt32();
                    }
                    else if (Settings.CurrentWorkout.Transitioning)
                    {
                        sb.Append("Transition");
                    }
                    else
                    {
                        sb.Append("Set ");
                        sb.Append(Settings.CurrentWorkout.CurrentSet.ToString());
                        sb.Append(" lift ");
                        sb.Append(Settings.CurrentWorkout.CurrentPartner.ToString());
                    }
                }
                catch (Exception ex)
                {
                    Mvx.Trace(ex.Message);
                }
            }
            else
            {
                sb.Append("Workout Complete");
                _workingout = false;
                BtnPausePlayText = "Start";
                Settings.ClearWorkout();
            }

            using (EventWaitHandle tmpEvent = new ManualResetEvent(false))
            {
                tmpEvent.WaitOne(TimeSpan.FromSeconds(1));
            }

            try
            {
				ButtonsEnabled = false;
                _tts.Play(sb.ToString(), FinishedTts);
            }
            catch (Exception ex)
            {
                Mvx.Trace(ex.Message);
                ResumeWorkout();
            }
            CurrentPosition = sb.ToString();
        }

        private void FinishedTts()
        {
			ButtonsEnabled = true;
            if (!_workoutComplete)
            {
                ResumeWorkout();
            }
            else
            {
                if (!Settings.IsPaid)
                    DisplayAd();
            }
        }

        private void PauseOrResume()
        {
            if (_playing)
            {
                PauseWorkout();
                BtnPausePlayText = "Resume";
            }
            else if (_workoutComplete)
            {
                Settings.ClearWorkout();
                _workoutComplete = false;
                DataStore.SaveSettings();
                BtnPausePlayText = "Pause";
                UpdateWorkout();
            }
            else if (_workoutReset)
            {
                _workoutReset = false;
                BtnPausePlayText = "Pause";
                UpdateWorkout();
            }
            else
            {
                BtnPausePlayText = "Pause";
                ResumeWorkout();
            }

            if (!Settings.CurrentWorkout.Transitioning)
                _workingout = !_workingout;
        }

        public ICommand StopAndSave
        {
			get { return new MvxCommand (PauseWorkout); }
        }

        private void ResetWorkout()
        {
            StopWorkout();
            ClockValue = 0;
            Settings.ClearWorkout();
            CurrentPosition = String.Empty;
            _workingout = false;
            _workoutComplete = false;
            Settings.CurrentWorkout.Transitioning = false;
            _workoutReset = true;
            BtnPausePlayText = "Start";
            DataStore.SaveSettings();
        }

        public void Workout_Timer_Tick(object stateInfo)
        {
            if (Settings.CurrentWorkout.SecondsElapsed >= MaxProgressValue)
            {
                NextAction();
            }
            else
            {
                Settings.CurrentWorkout.SecondsElapsed++;
                ClockValue = Settings.CurrentWorkout.SecondsElapsed;
            }
        }

        private void NextAction()
        {
            ClockValue = 0f;
            StopWorkout();

            if (Settings.CurrentWorkout.WarmingUp)
            {
                Settings.CurrentWorkout.WarmingUp = false;
                _workingout = true;
                MaxProgressValue = Settings.SetLengthSeconds.ToInt32();
            }

            if (Settings.CurrentWorkout.Transitioning)
            {
                Settings.CurrentWorkout.Transitioning = false;
                _workingout = true;
                MaxProgressValue = Settings.SetLengthSeconds.ToInt32();
            }

            //check partner count
            if (Settings.CurrentWorkout.CurrentPartner >= Settings.PartnerCount.ToInt32())
            {
                Settings.CurrentWorkout.CurrentPartner = 1;
                NextSet();
            }
            else
            {
                Settings.CurrentWorkout.CurrentPartner++;
            }

            if (Settings.SkipAfterLift)
            {
                _player.NextTrack();
            }

            UpdateWorkout();
            DataStore.SaveSettings();
        }

        private void NextSet()
        {
            if (Settings.CurrentWorkout.CurrentSet >= Settings.SetCount.ToInt32())
            {
                Settings.CurrentWorkout.CurrentSet = 1;
                NextStation();
            }
            else
            {
                Settings.CurrentWorkout.CurrentSet++;
            }
        }


        private void NextStation()
        {
            if (Settings.CurrentWorkout.CurrentStation >= Settings.StationCount.ToInt32())
            {
                Settings.CurrentWorkout.CurrentStation = 1;
                _workoutComplete = true;
            }
            else
            {
                Settings.CurrentWorkout.Transitioning = true;
                _workingout = false;
                Settings.CurrentWorkout.CurrentStation++;
                Settings.CurrentWorkout.SecondsElapsed = 0;
                Settings.CurrentWorkout.CurrentPartner = 0;
                MaxProgressValue = Settings.TransitionLengthSeconds.ToInt32();
            }
        }

        private void StopWorkout()
		{
			PauseWorkout();
            Settings.CurrentWorkout.SecondsElapsed = 0;
            
        }

        private void PauseWorkout()
        {
			DataStore.SaveSettings ();
            _workoutClock.Change(Timeout.Infinite, Timeout.Infinite);
            _player.Stop();
            _playing = false;
        }

        private void ResumeWorkout()
        {
            try
            {
                _workoutClock.Change(1000, 1000);
                _player.Start();
                _playing = true;
            }
            catch (Exception ex)
            {
                ReportError(ex.Message);
            }

        }

        private void DisplayAd()
        {
			if (!AdMobLoaded) {
				CloseCommand.Execute (null);
				string message = "ShowAppAd";
				_messenger.Publish(new DisplayMessage(this, message));
			} else {
				string message = "ShowAd";
				_messenger.Publish(new DisplayMessage(this, message));
			}
        }
    }
}