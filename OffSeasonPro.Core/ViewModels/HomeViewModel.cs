﻿using System.Windows.Input;
using Cirrious.MvvmCross.ViewModels;
using OffSeasonPro.Core.Models;

namespace OffSeasonPro.Core.ViewModels
{
    public class HomeViewModel
        : BaseViewModel
    {
        public HomeViewModel()
        {
          //  DataStore.CreateTts();
        }

        public Settings Settings
        {
            get { return DataStore.Settings; }
        }

        public ICommand ShowSettingsCommand
        {
            get { return new MvxCommand(() => ShowViewModel<SettingsViewModel>()); }
        }

        public ICommand ToggleActionCommand
        {
            get { return new MvxCommand(ToggleAction); }
        }

        private void ToggleAction()
        {
            if (!string.IsNullOrEmpty(Settings.SettingsValid()))
                ReportError("Invalid Settings. Please fix the following issues from within the settings " + Settings.SettingsValid());
            else
            {
                ShowViewModel<WorkoutViewModel>();
            }
        }
    }
}