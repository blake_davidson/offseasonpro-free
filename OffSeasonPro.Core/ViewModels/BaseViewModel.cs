using System.Windows.Input;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Email;
using Cirrious.MvvmCross.ViewModels;
using OffSeasonPro.Core.Interfaces;

namespace OffSeasonPro.Core.ViewModels
{
    public class BaseViewModel
        : MvxViewModel
    {
        public ICommand CloseCommand
        {
            get { return new MvxCommand(RequestClose); }
        }

        protected IDataStore DataStore
        {
            get { return Mvx.Resolve<IDataStore>(); }
        }

        protected void ReportError(string text)
        {
            Mvx.Resolve<IErrorReporter>().ReportError(text);
        }

        protected void ComposeEmail(string to, string subject, string body)
        {
            var task = Mvx.Resolve<IMvxComposeEmailTask>();
            task.ComposeEmail(to, null, subject, body, false);
        }

        protected void RequestClose()
        {
            var closer = Mvx.Resolve<IViewModelCloser>();
            closer.RequestClose(this);
        }
    }
}