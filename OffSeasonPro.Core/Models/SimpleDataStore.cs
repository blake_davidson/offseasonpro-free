using System;
using System.IO;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Cirrious.CrossCore;
using Cirrious.CrossCore.Platform;
using Cirrious.MvvmCross.Plugins.File;
using OffSeasonPro.Core.Interfaces;
using OffSeasonPro.Plugin.TextToSpeech;
using System.Collections.Generic;

namespace OffSeasonPro.Core.Models
{
    public class SimpleDataStore
        : IDataStore
    {
        private const string SettingsFileName = "Settings.xml";
        private const string SettingsResourceFileName = "Xml/Settings.xml";
        private readonly Object _playlist;

        private readonly ITextToSpeech _tts;
        private Settings _settings;

        public SimpleDataStore()
        {
            LoadSettings();
            _tts = Mvx.Resolve<ITextToSpeech>();
            _playlist = null;
        }

        public object Playlist
        {
            get { return _playlist; }
        }

        public void SaveSettings()
        {
            var fileService = Mvx.Resolve<IMvxFileStore>();

            fileService.WriteFile(SettingsFileName, (stream) =>
                {
                    var serializer = new XmlSerializer(typeof (Settings));
                    serializer.Serialize(stream, _settings);
                });
        }

		public void CreateTts(Action Finished, Action ErrorOccured)
        {
			var textsAndFilenames = new Dictionary<string,string> ();

            for (int i = 1; i < Settings.SetCount.ToInt32() + 1; i++)
            {
                for (int j = 1; j < Settings.PartnerCount.ToInt32() + 1; j++)
                {
                    int i1 = i;
                    int j1 = j;
					textsAndFilenames.Add("Set " + i1 + " lift " + j1, "Set" + i1 + "lift" + j1 + ".mp3");
                }
            }

			textsAndFilenames.Add("Warm up ", "Warmup.mp3");

			textsAndFilenames.Add("Workout Complete", "WorkoutComplete.mp3"); 

			textsAndFilenames.Add("Transition", "Transition.mp3");

			ThreadPool.QueueUserWorkItem (delegate {
				_tts.TextToSpeech (textsAndFilenames, Finished, ErrorOccured);
			});
        }


        public Settings Settings
        {
            get { return _settings; }
        }


        public void ReloadSettings()
        {
            LoadSettings();
        }

        private void LoadSettings()
        {
            var fileService = Mvx.Resolve<IMvxFileStore>();
            if (!fileService.TryReadBinaryFile(SettingsFileName, LoadSettingsFrom))
            {
                var resourceLoader = Mvx.Resolve<IMvxResourceLoader>();
                resourceLoader.GetResourceStream(SettingsResourceFileName,
                                                 (inputStream) => LoadSettingsFrom(inputStream));
            }
        }

        private bool LoadSettingsFrom(Stream inputStream)
        {
            try
            {
                XDocument loadedData = XDocument.Load(inputStream);
                if (loadedData.Root == null)
                    return false;

                using (XmlReader reader = loadedData.Root.CreateReader())
                {
                    var settings = (Settings) new XmlSerializer(typeof (Settings)).Deserialize(reader);
                    _settings = settings;
                    return true;
                }
            }
            catch (Exception exception)
            {
                Trace.Error("Problem loading settings {0}", exception.ToString());
                return false;
            }
        }
    }
}