﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.File;
using Cirrious.MvvmCross.ViewModels;
using OffSeasonPro.Core.ApplicationObjects;
using OffSeasonPro.Core.Interfaces;
using OffSeasonPro.Core.Models;
using OffSeasonPro.Core.ViewModels;

namespace OffSeasonPro.Core
{
    public class App
        : MvxApplication
    {
        public App()
        {
            PluginLoader.Instance.EnsureLoaded();
            Cirrious.MvvmCross.Plugins.ResourceLoader.PluginLoader.Instance.EnsureLoaded();
           // Plugin.MusicPlayer.PluginLoader.Instance.EnsureLoaded();
            Plugin.TextToSpeech.PluginLoader.Instance.EnsureLoaded();
            Cirrious.MvvmCross.Plugins.Messenger.PluginLoader.Instance.EnsureLoaded();

            // set up the model
            var dataStore = new SimpleDataStore();
            Mvx.RegisterSingleton<IDataStore>(dataStore);

            //Set up error system
            InitalizeErrorSystem();

            // set the start object
            RegisterAppStart<HomeViewModel>();
        }

        private void InitalizeErrorSystem()
        {
            var errorHub = new ErrorApplicationObject();
            Mvx.RegisterSingleton<IErrorReporter>(errorHub);
            Mvx.RegisterSingleton<IErrorSource>(errorHub);
        }
    }
}