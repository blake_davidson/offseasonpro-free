﻿using System;
using OffSeasonPro.Core.Models;

namespace OffSeasonPro.Core.Interfaces
{
    public interface IErrorSource
    {
        event EventHandler<ErrorEventArgs> ErrorReported;
    }
}