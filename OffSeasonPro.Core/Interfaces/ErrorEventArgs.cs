﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OffSeasonPro.Core.Interfaces
{
    public class ErrorEventArgs : EventArgs
    {
        public string Message { get; private set; }

        public ErrorEventArgs(string message)
        {
            Message = message;
        }
    }
}
