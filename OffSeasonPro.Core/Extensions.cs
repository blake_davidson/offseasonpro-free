﻿using System;
using OffSeasonPro.Core.Models;

namespace OffSeasonPro.Core
{
    public static class Extensions
    {
        public static int ToInt32(this string str)
        {
            try
            {
                return Convert.ToInt32(str);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static void ClearWorkout(this Settings s)
        {
            s.CurrentWorkout.CurrentPartner = 1;
            s.CurrentWorkout.CurrentSet = 1;
            s.CurrentWorkout.CurrentStation = 1;
            s.CurrentWorkout.SecondsElapsed = 0;

            if (s.StartupLength.ToInt32() > 0)
            {
                s.CurrentWorkout.WarmingUp = true;
                s.CurrentWorkout.CurrentPartner = 0;
            }
        }

        public static bool InWorkout(this Workout workout)
        {
            if (workout.CurrentPartner > 1 || workout.CurrentSet > 1 || workout.CurrentStation > 1 ||
                workout.SecondsElapsed > 0)
                return true;
            return false;
        }

        public static string SettingsValid(this Settings s)
        {
            var sb = new System.Text.StringBuilder();
            if (s.PartnerCount.ToInt32() == 0)
            {
                sb.AppendLine("Partner count must be greater than 0");
            }
            if (s.SetCount.ToInt32() == 0)
            {
                sb.AppendLine("Set Count must be greater than 0");
            }
            if (s.SetLengthSeconds.ToInt32() == 0)
            {
                sb.AppendLine("Set Length must be greater than 0");
            }
            if (s.StationCount.ToInt32() == 0)
            {
                sb.AppendLine("Station Count must be greater than 0");
            }
             if (s.TransitionLengthSeconds.ToInt32() == 0)
             {
                 sb.AppendLine("Transition Length must be greater than 0");
             }
           
            return sb.ToString();
        }
    }
}