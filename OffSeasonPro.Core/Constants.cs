﻿namespace OffSeasonPro.Core
{
    public static class Constants
    {
        public const string GeneralNamespace = "OffSeasonPro";
        public const string Shared = "Shared";
        public const string RootFolderForResources = "ProResources/Text";
    }
}