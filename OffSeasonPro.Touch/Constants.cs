using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace OffSeasonPro.Touch
{
    public static class Constants
    {
        public static string Background
        {
            get
            {
                return IsTallScreen ? "Backgrounds/OffSeason_Pro_ChalkBoard-5" : "Backgrounds/OffSeason_Pro_ChalkBoard";
            }
        }
 
        public static string ConcreteBackground
        {
            get { return IsTallScreen ? "Backgrounds/OffSeason_Pro_Concrete-5" : "Backgrounds/OffSeason_Pro_Concrete"; }
        }

        public static string ChalkFont = "rayando";
        public static string PrincetownNormalFont = "Princetown";
        public static string PrincetownSoldiFont = "PrincetownSolid-Normal";
		public static string BebasNeueBold = "BebasNeueBold";
		public static string BebasNeueRegular = "BebasNeueRegular";

        public static bool IsTallScreen
        {
            get
            {
                return UIScreen.MainScreen.Bounds.Height >= 568;
            }
        }

    }
}