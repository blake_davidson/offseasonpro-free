using System;
using System.Drawing;
using System.Linq;
using Cirrious.CrossCore.Platform;
using MonoTouch.AudioToolbox;
using MonoTouch.CoreGraphics;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Touch.Platform;
using Cirrious.MvvmCross.ViewModels;
using OffSeasonPro.Core.Interfaces;
//using OffSeasonPro.Plugin.MusicPlayer;
using OffSeasonPro.Touch.Views;
using OffSeasonPro.Plugin.MusicPlayerPlugin;

namespace OffSeasonPro.Touch
{
    [Register("AppDelegate")]
    public partial class AppDelegate
        : MvxApplicationDelegate
    {
        UIWindow _window;

        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            _window = new UIWindow(UIScreen.MainScreen.Bounds);

            var presenter = new OffSeasonProPresenter(this, _window);
            var setup = new Setup(this, presenter);
            setup.Initialize();

            var startup = Mvx.Resolve<IMvxAppStart>();
            startup.Start();

            _window.BackgroundColor =  UIColor.FromPatternImage(UIImage.FromBundle(Constants.Background));

            _window.MakeKeyAndVisible();

            UINavigationBar.Appearance.TintColor = UIColor.FromRGB(125, 142, 142); // Gray
            UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes()
                {
                    Font = UIFont.FromName(Constants.ChalkFont, 25f),
                    TextColor = UIColor.FromRGB(240, 240, 240)
                });

            return true;
        }

		public override void WillEnterForeground(UIApplication application)
		{

		}


		public override void OnResignActivation(UIApplication application)
		{
				var rootVC = _window.RootViewController;
				var topVC = rootVC.ChildViewControllers.Last ();


			if (topVC is WorkoutView) {
				if(((WorkoutView)topVC).ViewModel.Playing){
					((WorkoutView)topVC).ViewModel.PauseResumeWorkout.Execute (null);
				}	
			}
		}

        public override void DidEnterBackground(UIApplication application)
        {
           
			 base.DidEnterBackground(application);

        }
    }
}