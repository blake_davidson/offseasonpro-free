using System;
using MBProgressHUD;
using MonoTouch.UIKit;
using Cirrious.MvvmCross.Touch.Views;
using OffSeasonPro.Touch.Views;
using OffSeasonPro.Core.ViewModels;
using System.Windows.Input;

namespace OffSeasonPro.Touch
{
	public class BindableProgress
	{
		private MTMBProgressHUD _progress;
		private readonly UIView _parent;
		private readonly string _labelText;
		private readonly bool _closeViewWhenFinished;
		private readonly ICommand _closeCommand;

		public BindableProgress (UIViewController parent, string labelText, bool closeViewWhenFinished, ICommand closeCommand)
		{
			_parent = parent.View;
			_labelText = labelText;
			_closeViewWhenFinished = closeViewWhenFinished;
			_closeCommand = closeCommand;
		}

		public bool Visible {
			get { return _progress != null; }
			set {
				if (Visible == value)
					return;

				if (value) {
					_progress = new MTMBProgressHUD (_parent) {
						LabelText = _labelText,
						RemoveFromSuperViewOnHide = true
					};
					_parent.AddSubview (_progress);
					_progress.Show (true);
				} else {
					_progress.Hide (true);
					_progress = null;

					if (_closeViewWhenFinished) {
						_closeCommand.Execute (null);
					}
						
				}
			}
		}
	}
}

