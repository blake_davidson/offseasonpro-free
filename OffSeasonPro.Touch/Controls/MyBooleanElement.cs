using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using CrossUI.Touch.Dialog.Elements;
using MonoTouch.UIKit;

namespace OffSeasonPro.Touch.Controls
{
    public class MyBooleanElement : BooleanElement
    {
        public MyBooleanElement(string caption, bool value)
            : base(caption, value)
        {
        }

        protected override UITableViewCell GetCellImpl(UITableView tv)
        {
            var cell = base.GetCellImpl(tv);
            var color = new UIColor(255, 255, 255, 0.1f);
            cell.BackgroundColor = color;

            if (cell.TextLabel != null)
            {
				cell.TextLabel.Font = UIFont.FromName(Constants.BebasNeueBold, 20f);
                cell.TextLabel.ShadowColor = UIColor.Black;
                cell.TextLabel.TextColor = UIColor.FromRGB(202, 202, 202);
                cell.TextLabel.ShadowOffset = new SizeF(.60f, .60f);
            }


            return cell;
        }
    }
}