// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the Xcode designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;

namespace OffSeasonPro.Touch.Views
{
	[Register ("WorkoutView")]
	partial class WorkoutView
	{
		[Outlet]
		MonoTouch.UIKit.UILabel lblCurrentPosition { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnPauseResume { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnReset { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnBack { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (lblCurrentPosition != null) {
				lblCurrentPosition.Dispose ();
				lblCurrentPosition = null;
			}

			if (btnPauseResume != null) {
				btnPauseResume.Dispose ();
				btnPauseResume = null;
			}

			if (btnReset != null) {
				btnReset.Dispose ();
				btnReset = null;
			}

			if (btnBack != null) {
				btnBack.Dispose ();
				btnBack = null;
			}
		}
	}
}
