using System.Collections.Generic;
using System.Drawing;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Dialog.Touch;
using MonoTouch.ObjCRuntime;
using MonoTouch.UIKit;
using OffSeasonPro.Core.ViewModels;
using CrossUI.Touch.Dialog.Elements;
using Cirrious.MvvmCross.Touch.Views;
using OffSeasonPro.Touch.Controls;

namespace OffSeasonPro.Touch.Views
{
	public class SettingsView
        : MvxDialogViewController
        , IMvxModalTouchView
	{
		private BindableProgress _bindableProgress;

		public SettingsView ()
            : base (UITableViewStyle.Grouped, null, true)
		{
		}

		public SettingsView (RootElement root, bool pushing)
            : base (UITableViewStyle.Grouped, root, pushing)
		{
		}

		public SettingsView (UITableViewStyle style, RootElement root, bool pushing)
            : base (style, root, pushing)
		{
		}

		public new SettingsViewModel ViewModel {
			get { return (SettingsViewModel)base.ViewModel; }
			set { base.ViewModel = value; }
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// ios7 layout
			if (RespondsToSelector (new Selector ("edgesForExtendedLayout")))
				EdgesForExtendedLayout = UIRectEdge.None;

			_bindableProgress = new BindableProgress (this, "Saving Settings", true, ViewModel.CloseCommand);

			var attr = new UITextAttributes ();
			attr.TextColor = UIColor.LightGray;
			attr.TextShadowColor = UIColor.Black;
			attr.TextShadowOffset = new UIOffset (.80f, .80f);
			                      
			var btnSave = new UIBarButtonItem ("Save", UIBarButtonItemStyle.Plain, (s, e) => ViewModel.SaveSettings.Execute (null));
			btnSave.SetTitleTextAttributes (attr, UIControlState.Normal);

			var btnCancel = new UIBarButtonItem ("Cancel", UIBarButtonItemStyle.Plain, (s, e) => ViewModel.CancelSave.Execute (null));
			btnCancel.SetTitleTextAttributes (attr, UIControlState.Normal);

			this.SetToolbarItems (new UIBarButtonItem[] {
				btnSave	
				, new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace) { Width = 50 }
				, btnCancel
			}, false);

			this.NavigationController.Toolbar.SetBackgroundImage (UIImage.FromBundle ("toolbarBg"), UIToolbarPosition.Any, UIBarMetrics.Default);

			View.BackgroundColor = UIColor.FromPatternImage (UIImage.FromBundle (Constants.Background));

			var set = this.CreateBindingSet<SettingsView, SettingsViewModel> ();
			set.Bind (_bindableProgress).For (b => b.Visible).To (vm => vm.IsBusy);
			set.Apply ();

		}

		public override bool PrefersStatusBarHidden ()
		{
			return true;
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			this.NavigationController.ToolbarHidden = false;

			ResetDisplay ();
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			this.NavigationController.ToolbarHidden = true;
		}

		UIView dismiss;

		public override UIView InputAccessoryView {
			get {
				if (dismiss == null) {
					dismiss = new UIView (new RectangleF (0, 0, 320, 27));
					dismiss.BackgroundColor = UIColor.Black;
					var dismissBtn = new UIButton (new RectangleF (255, 2, 58, 23)) { BackgroundColor = UIColor.White };
					dismissBtn.SetTitle (@"Done", UIControlState.Normal);
					dismissBtn.BackgroundColor = UIColor.Black;
					dismissBtn.SetTitleColor (UIColor.White, UIControlState.Normal);
					dismissBtn.TouchDown += delegate {
						View.EndEditing (true);
					};

					dismiss.AddSubview (dismissBtn);
				}
				return dismiss;
			}
		}

		private void ResetDisplay ()
		{
			var section = new Section (new UIImageView (UIImage.FromBundle ("Header@2x.png")));
			var elements = new List<EntryElement> {
				new MyEntryElement ("Partners") {
					KeyboardType = UIKeyboardType.NumberPad,
					MaxLength = 2,
					Alignment = UITextAlignment.Right,
				}.Bind (this, "Value Settings.PartnerCount"),
				new MyEntryElement ("Sets") {
					KeyboardType = UIKeyboardType.NumberPad,
					MaxLength = 2,
					Alignment = UITextAlignment.Right
				}.Bind (this, "Value Settings.SetCount"),
				new MyEntryElement ("Stations") {
					KeyboardType = UIKeyboardType.NumberPad,
					MaxLength = 2,
					Alignment = UITextAlignment.Right
				}.Bind (this, "Value Settings.StationCount"),
				new MyEntryElement ("Warmup Length") {
					KeyboardType = UIKeyboardType.NumberPad,
					MaxLength = 3,
					Alignment = UITextAlignment.Right
				}.Bind (this, "Value Settings.StartupLength"),
				new MyEntryElement ("Set Length") {
					KeyboardType = UIKeyboardType.NumberPad,
					MaxLength = 3,
					Alignment = UITextAlignment.Right
				}.Bind (this, "Value Settings.SetLengthSeconds"),
				new MyEntryElement ("Transition Length") {
					KeyboardType = UIKeyboardType.NumberPad,
					MaxLength = 3,
					Alignment = UITextAlignment.Right
				}.Bind (this, "Value Settings.TransitionLengthSeconds")
                        
			};
			section.AddAll (elements);
			section.Add (
				new MyBooleanElement ("Change track on each lift", ViewModel.Settings.SkipAfterLift).Bind (this,

					"Value Settings.SkipAfterLift"));
			if (ViewModel.Settings.UsePhotoAlbumForBgImage) {
				section.Add (new MyBadgeElement (UIImage.FromFile (ViewModel.Settings.WorkoutBackgroundImage), "Background Image", () => ViewModel.ShowBackgroundChooser.Execute (null)));
			} else {
				section.Add (new MyBadgeElement (string.IsNullOrEmpty (ViewModel.Settings.WorkoutBackgroundImage)
					? UIImage.FromBundle (Constants.Background) : UIImage.FromBundle (ViewModel.Settings.WorkoutBackgroundImage),
					"Background Image", () => ViewModel.ShowBackgroundChooser.Execute (null)));
			}

			Root = new RootElement ("Settings") { 
				section,
			};

			//Add background
			TableView.BackgroundView = null;
			TableView.BackgroundColor = UIColor.Clear;
     
		}
	}
}

