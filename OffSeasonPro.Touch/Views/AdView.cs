using System;
using System.Drawing;
using Cirrious.MvvmCross.Binding.BindingContext;
using MonoTouch.CoreGraphics;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Cirrious.MvvmCross.Touch.Views;
using OffSeasonPro.Core;

namespace OffSeasonPro.Touch.Views
{
    [Register("AdView")]
	public class AdView : UIView
    {

		public AdView(RectangleF frame) :base(frame)
		{
			Initialize ();
		}

        void Initialize()
        {
			BackgroundColor = UIColor.FromPatternImage(UIImage.FromBundle("OSP_AD"));
			var btn = new UIButton(new RectangleF(0, 0, 50, 50));
            btn.SetTitle("X", UIControlState.Normal);
			btn.Font = UIFont.FromName (Constants.ChalkFont, 36);
			btn.TouchUpInside += (object sender, EventArgs e) => { Hidden = true; };
			Add (btn);
            WireUpTapGestureRecognizer();
        }

        protected void WireUpTapGestureRecognizer()
        {
            // create a new tap gesture
            UITapGestureRecognizer tapGesture = null;

            NSAction action = () => UIApplication.SharedApplication.OpenUrl(
                new NSUrl("https://itunes.apple.com/us/app/offseason-pro/id808595637?ls=1&mt=8"));

            tapGesture = new UITapGestureRecognizer(action);
            // configure it
			tapGesture.NumberOfTapsRequired = 1;
            // add the gesture recognizer to the view
			AddGestureRecognizer(tapGesture);
        }
    }
}