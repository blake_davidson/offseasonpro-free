
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using OffSeasonPro.Core.ViewModels;
using Cirrious.MvvmCross.Touch.Views;
using MonoTouch.MediaPlayer;

namespace OffSeasonPro.Touch
{
	public partial class PlaylistView 
		: MvxViewController
	{


		public PlaylistView () : base ("PlaylistView", null)
		{
		}

		public new PlaylistViewModel ViewModel
		{
			get { return (PlaylistViewModel)base.ViewModel; }
			set { base.ViewModel = value; }
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Perform any additional setup after loading the view, typically from a nib.
			ShowMediaPicker ();
		}

		private void ShowMediaPicker()
		{

			MPMediaPickerController picker = new MPMediaPickerController(MPMediaType.Music);
			picker.Delegate = new MPMediaPickerControllerDelegate ();
			picker.AllowsPickingMultipleItems = true;
			picker.Prompt = @"Select Songs to Play";
			this.PresentViewController (picker, true, null);

		}
	}

	public class MediaPickerDelegate
		: MPMediaPickerControllerDelegate
	{
		UIViewController _viewController();

		public MediaPickerDelegate(UIViewController v)
		{
			_viewController = v;
		}

		public override void MediaItemsPicked (MPMediaPickerController sender, MPMediaItemCollection mediaItemCollection)
		{
			_viewController = sender ();
			if (mediaItemCollection) {
			//Handle Collection
			} else {
			//Play Single Track
			}
			
		}

		public override void MediaPickerDidCancel (MPMediaPickerController sender)
		{
			
		}
	}
}

