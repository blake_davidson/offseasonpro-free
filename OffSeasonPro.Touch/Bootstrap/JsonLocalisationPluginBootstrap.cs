using Cirrious.CrossCore.Plugins;

namespace OffSeasonPro.Touch.Bootstrap
{
    public class JsonLocalisationPluginBootstrap
        : MvxPluginBootstrapAction<Cirrious.MvvmCross.Plugins.JsonLocalisation.PluginLoader>
    {
    }
}