﻿using Cirrious.CrossCore;
using MonoTouch.UIKit;
using OffSeasonPro.Core.Interfaces;

namespace OffSeasonPro.Touch
{
    public class ErrorDisplayer
    {
        public ErrorDisplayer()
        {
            var source = Mvx.Resolve<IErrorSource>();
            source.ErrorReported += (sender, args) => ShowError(args.Message);
        }

        private void ShowError(string message)
        {
            var errorView = new UIAlertView("Offseason Pro", message, null, "OK", null);
            errorView.Show();
        }
    }
}
