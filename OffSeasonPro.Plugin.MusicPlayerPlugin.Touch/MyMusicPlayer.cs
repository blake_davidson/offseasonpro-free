using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Cirrious.CrossCore;
using Cirrious.CrossCore.Exceptions;
using Cirrious.CrossCore.Platform;
using MonoTouch.Foundation;
using MonoTouch.MediaPlayer;
using MonoTouch.UIKit;

namespace OffSeasonPro.Plugin.MusicPlayerPlugin.Touch
{
    public class MyMusicPlayer
    {       
        public const string ResScheme = "res:";
        private static MyMusicPlayer _instance;

        private MyMusicPlayer()
        {
            Player = new MPMusicPlayerController { RepeatMode = MPMusicRepeatMode.All };
            _playlistCount = 0;
            GetPlaylist();
        }

        public MPMusicPlayerController Player { get; set; }

        private int _playlistCount;
        public int PlaylistCount
        {
            get { return _playlistCount; }
        }

        public static MyMusicPlayer Instance
        {
            get { return _instance ?? (_instance = new MyMusicPlayer()); }
        }

        private void GetPlaylist()
        {
            var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var localPath = Path.Combine(documentsPath, @"Json");
            var jsonFile = Path.Combine(localPath, @"Playlist.json");
            //Create Json Directory if one does not exist.
            if (!Directory.Exists(localPath))
            {
                Directory.CreateDirectory(localPath);
            }

            if (File.Exists(jsonFile))
            {
                var file = GetTextResource(jsonFile);

                if (file != null && !String.IsNullOrWhiteSpace(jsonFile))
                {
                    var jsonConvert = Mvx.Resolve<IMvxJsonConverter>();
                    try
                    {
                        var items = jsonConvert.DeserializeObject<List<string>>(file);

                        if (items != null && items.Count > 0)
                        {
                            var songs = new List<MPMediaItem>();
                            foreach (var item in items)
                            {
                                var mq = new MPMediaQuery();
                                mq.AddFilterPredicate(MPMediaPropertyPredicate.PredicateWithValue(NSObject.FromObject(item), MPMediaItem.PersistentIDProperty));

                                if (mq.Items.Length > 0)
                                {
                                    songs.Add(mq.Items[0]);
                                }

                            }
                            Player.SetQueue(new MPMediaItemCollection(songs.ToArray()));
                            _playlistCount = songs.Count;
                            Mvx.Trace(MvxTraceLevel.Diagnostic, "Loaded " + songs.Count + " songs from storage.");
                        }
                    }
                    catch (Exception ex)
                    {
                        Mvx.Trace(MvxTraceLevel.Error, "Error loading playlist " + ex.Message, null);
                    }
                }
            }
        }

        public string GetTextResource(string resourcePath)
        {
            try
            {
                string text = null;
                GetResourceStream(resourcePath, (stream) =>
                {
                    if (stream == null)
                        return;

                    using (var textReader = new StreamReader(stream))
                    {
                        text = textReader.ReadToEnd();
                    }
                });
                return text;
            }

            catch (Exception ex)
            {
                throw ex.MvxWrap("Cannot load resource {0}", resourcePath);
            }
        }


        public void GetResourceStream(string resourcePath, Action<Stream> streamAction)
        {

            if (!TryReadBinaryFile(resourcePath, (stream) =>
            {
                streamAction(stream);
                return true;
            }))
                throw new MvxException("Failed to read file {0}", resourcePath);
        }

        public bool TryReadBinaryFile(string path, Func<Stream, bool> readMethod)
        {
            return TryReadFileCommon(path, readMethod);
        }

        private bool TryReadFileCommon(string path, Func<Stream, bool> streamAction)
        {
            var fullPath = FullPath(path);
            if (!System.IO.File.Exists(fullPath))
            {
                return false;
            }

            using (var fileStream = System.IO.File.OpenRead(fullPath))
            {
                return streamAction(fileStream);
            }
        }

        protected string FullPath(string path)
        {
            if (path.StartsWith(ResScheme))
                return path.Substring(ResScheme.Length);

            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), path);
        }

        private void SavePlaylist(IEnumerable<MPMediaItem> items)
        {
            var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var localPath = Path.Combine(documentsPath, @"Json");
            var jsonFile = Path.Combine(localPath, @"Playlist.json");
            var jsonConvert = Mvx.Resolve<IMvxJsonConverter>();
            try
            {
                var itemIDs = items.Select(m => m.PersistentID.ToString(CultureInfo.InvariantCulture)).ToList();
                var file = jsonConvert.SerializeObject(itemIDs);
                File.WriteAllText(jsonFile, file);
            }
            catch (Exception ex)
            {
                Mvx.Trace(MvxTraceLevel.Warning, "Error saving playlist " + ex.Message, null);
            }
            
        }

        public class MediaPickerDelegate : MPMediaPickerControllerDelegate
        {
            readonly MyMusicPlayer _musicPlayer;
            readonly UIViewController _viewController;

            public MediaPickerDelegate(MyMusicPlayer musicPlayer, UIViewController viewController)
            {
                _musicPlayer = musicPlayer;
                _viewController = viewController;
            }

            public override void MediaItemsPicked(MPMediaPickerController sender, MPMediaItemCollection mediaItemCollection)
            {
                _musicPlayer.Player.SetQueue(mediaItemCollection);
                _musicPlayer.SavePlaylist(mediaItemCollection.Items);
                _viewController.DismissViewController(true, null);
            }

            public override void MediaPickerDidCancel(MPMediaPickerController sender)
            {
                _viewController.DismissViewController(true, null);
            }
        }

    }
}