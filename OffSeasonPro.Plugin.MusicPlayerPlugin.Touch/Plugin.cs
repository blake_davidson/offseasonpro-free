using Cirrious.CrossCore;
using Cirrious.CrossCore.Plugins;
using MonoTouch.UIKit;
using Cirrious.CrossCore.Platform;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.MvvmCross.Touch.Platform;

namespace OffSeasonPro.Plugin.MusicPlayerPlugin.Touch
{
    public class Plugin
        : IMvxPlugin
    {
        public void Load()
        {
            Mvx.RegisterSingleton<IMusicPlayer>(new MvxTouchMusicPlayer());
        }
    }
}