using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace OffSeasonPro.Plugin.MusicPlayerPlugin.Touch
{
    public class MvxTouchMusicPlayer : IMusicPlayer
    {
        public MyMusicPlayer Mp;

        public MvxTouchMusicPlayer()
        {
            Mp = MyMusicPlayer.Instance;
        }

        public void Start()
        {
            Mp.Player.Play();
        }

        public void Stop()
        {
            Mp.Player.Pause();
        }

        public void NextTrack()
        {
            Mp.Player.SkipToNextItem();
        }

    }
}