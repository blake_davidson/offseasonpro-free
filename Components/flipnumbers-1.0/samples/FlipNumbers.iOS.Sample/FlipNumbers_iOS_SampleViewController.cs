using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using FlipNumbers;

namespace FlipNumbers.iOS.Sample
{
	public partial class FlipNumbers_iOS_SampleViewController : UIViewController
	{
		FlipNumbersView flipView;
		Random rnd;

		public FlipNumbers_iOS_SampleViewController () : base ("FlipNumbers_iOS_SampleViewController", null)
		{
		}
		
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			rnd = new Random ((int) DateTime.UtcNow.Ticks);
			
			flipView = new FlipNumbersView (digitsCount: 4);
//			flipView.FlipDuration = 1.0;

			flipView.Value = rnd.Next (9999);

			View.AddSubview (flipView);

			flipView.Center = View.Center;
		}

		partial void OnFlipButtonTouched ()
		{
			flipView.SetValue (rnd.Next (9999), animated: true);
		}
	}
}

